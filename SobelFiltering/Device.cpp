#include <CL/cl.h>
#include <iostream>
#include <vector>
#include <map>
#include <fstream>
#include <streambuf>
#include "Device.h"

DeviceOCL::DeviceOCL(cl_device_id deviceId)
{
	_deviceId = deviceId;
	_context = clCreateContext(nullptr, 1, &deviceId, nullptr, nullptr, nullptr);
	_queue = clCreateCommandQueue(_context, deviceId, 0, nullptr);
}

DeviceOCL::~DeviceOCL()
{
	for (auto& [programName, program] : _programs)
	{
		clReleaseProgram(program);
	}
	clReleaseCommandQueue(_queue);
	clReleaseContext(_context);
	clReleaseDevice(_deviceId);
}

DevicePool::DevicePool()
{
	GetDevices();
}

std::string DevicePool::GetPlatformName(cl_platform_id platformID)
{
	char platformName[1024];
	clGetPlatformInfo(platformID, CL_PLATFORM_NAME, 1024, platformName, nullptr);
	return std::string(platformName);
}

std::string DevicePool::GetDeviceName(cl_device_id platformID)
{
	char deviceName[1024];
	clGetDeviceInfo(platformID, CL_DEVICE_NAME, 1024, deviceName, nullptr);
	return std::string(deviceName);
}

bool DevicePool::GetDevices()
{
	cl_uint platformIdCount = 0;
	clGetPlatformIDs(0, nullptr, &platformIdCount);

	if (platformIdCount == 0)
	{
		std::cerr << "No OpenCL platform found" << std::endl;
		return 1;
	}

	std::cout << "Found " << platformIdCount << " platform(s)" << std::endl;

	std::vector<cl_platform_id> platformIds(platformIdCount);
	clGetPlatformIDs(platformIdCount, platformIds.data(), nullptr);

	for (cl_uint platformIndex = 0; platformIndex < platformIdCount; platformIndex++)
	{
		std::cout << "\t platform(" << (platformIndex + 1) << ") : " << GetPlatformName(platformIds[platformIndex]) << std::endl;
		cl_uint deviceIdCount = 0;
		clGetDeviceIDs(platformIds[platformIndex], CL_DEVICE_TYPE_GPU, 0, nullptr, &deviceIdCount);

		if (deviceIdCount == 0)
		{
			std::cerr << "No OpenCL GPU devices found for this platform" << std::endl;
			continue;
		}

		std::cout << "Found " << deviceIdCount << " GPU device(s)" << std::endl;

		std::vector<cl_device_id> deviceIds(deviceIdCount);
		clGetDeviceIDs(platformIds[platformIndex], CL_DEVICE_TYPE_ALL, deviceIdCount, deviceIds.data(), nullptr);

		for (cl_uint deviceIndex = 0; deviceIndex < deviceIdCount; deviceIndex++)
		{
			std::cout << "\t device(" << (deviceIndex + 1) << ") : " << GetDeviceName(deviceIds[deviceIndex]) << std::endl;
			RegisterDevice(deviceIds[deviceIndex]);
		}
	}
	return true;
}

void DevicePool::RegisterDevice(cl_device_id deviceId)
{
	std::shared_ptr<DeviceOCL> device = std::make_shared<DeviceOCL>(deviceId);
	_devices.emplace_back(device);
}

std::shared_ptr<DeviceOCL> DevicePool::operator[](uint32 deviceIndex)
{
	if (deviceIndex >= _devices.size())
		return nullptr;
	return _devices[deviceIndex];
}

uint32 DevicePool::DeviceCount() const
{
	return (uint32)_devices.size();
}

bool DeviceOCL::ReadProgramFile(const std::string& programFile, std::string& stringBuffer)
{
	std::ifstream file(programFile.c_str());
	
	if (!file.good()) return false;
	
	file.seekg(0, std::ios::end);
	stringBuffer.reserve(file.tellg());
	file.seekg(0, std::ios::beg);

	stringBuffer.assign((std::istreambuf_iterator<char>(file)),
		std::istreambuf_iterator<char>());
	return true;
}

bool DeviceOCL::RegisterProgram(const std::string& programName, const std::string& programFile)
{
	if (_programs.find(programName) != _programs.end()) return true;
	std::string stringBuffer;
	if (!ReadProgramFile(programFile, stringBuffer)) return false;
	cl_int error;
	//std::cout << stringBuffer << std::endl;
	const char* programChar = stringBuffer.c_str();
	cl_program program = clCreateProgramWithSource(_context, 1, (const char**) &programChar, NULL, &error);
	if (error != CL_SUCCESS) return false;
	if (clBuildProgram(program, 1, &_deviceId, nullptr, nullptr, nullptr) != CL_SUCCESS) return false;
	_programs[programName] = program;
	return true;
}

cl_program DeviceOCL::GetProgram(const std::string& programName)
{
	if (_programs.find(programName) == _programs.end()) return nullptr;
	return _programs[programName];
}

cl_command_queue DeviceOCL::GetDeviceQueue() const
{
	return _queue;
}

cl_context DeviceOCL::GetDeviceContext() const
{
	return _context;
}

std::string DeviceOCL::GetDeviceName() const
{
	char deviceName[1024];
	clGetDeviceInfo(_deviceId, CL_DEVICE_NAME, 1024, deviceName, nullptr);
	return std::string(deviceName);
}
