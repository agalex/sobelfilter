#ifndef DEVICE_H
#define DEVICE_H

using uint32 = unsigned int;
class DevicePool;

class DeviceOCL
{
public:
	using ProgramPool = std::map<std::string, cl_program>;

	explicit DeviceOCL(cl_device_id deviceId);

	/**
	 * Getter for device context
	 *
	 * @return The device context 
	 */
	cl_context GetDeviceContext() const;

	/**
	 * Getter for the device queue
	 *
	 * @return The OpenCL command queue
	 */
	cl_command_queue GetDeviceQueue() const;

	/**
	 * Register an OpenCL program from source file.
	 *
	 * @param programName The program name that we wish the program to be registered with.
	 * @param programFile The file containing the OpenCL source.
	 * @return True if the registration was successful.
	 */
	bool RegisterProgram(const std::string& programName, const std::string& programFile);

	/**
	 * Get the program handle for use by OpenCL commands.
	 *
	 * @param programName The name of the program.
	 * @return The output program.
	 */
	cl_program GetProgram(const std::string& programName);

	/**
	 * Get the name of the device.
	 *
	 * @return The device name.
	 */
	std::string GetDeviceName() const;

	virtual ~DeviceOCL();
	
private:

	/**
	 * Read source from file and store it in a string.
	 *
	 * @param programFile The file containing the source.
	 * @param stringBuffer The string containing the source.
	 * @return If the file was read successfully.
	 */
	bool ReadProgramFile(const std::string& programFile, std::string& stringBuffer);

	ProgramPool _programs;		/**< The programs the device holds. */
	cl_device_id _deviceId;		/**< The OpenCL device Id. */
	cl_context _context;		/**< The OpenCL device context. */
	cl_command_queue _queue;	/**< The OpenCL device queue. */
};

class DevicePool
{
public:
	using DevicesOCL = std::vector<std::shared_ptr<DeviceOCL>>;
	DevicePool();

	/**
	 * operator that get a shared pointer of the device.
	 *
	 * @param deviceIndex The index of the device. Must be of a valid range.
	 */
	std::shared_ptr<DeviceOCL> operator[](uint32 deviceIndex);

	/**
	 * Get the number of registered devices.
	 *
	 * @return The number of registered devices.
	 */
	uint32 DeviceCount() const;

	/**
	 * Get the platform name (e.g. NVIDIA, AMD, Intel)
	 *
	 * @param platformID The OpenCL platform id.
	 * @return The name of the platform.
	 */
	static std::string GetPlatformName(cl_platform_id platformID);

	/**
	 * Get the device name.
	 *
	 * @param deviceID The OpenCL device id.
     * @return The name of the device.
	 */
	static std::string GetDeviceName(cl_device_id deviceID);
	
private:

	/**
	 * Registers a device to the pool of GPU devices.
	 *
	 * @param deviceId The OpenCL deviceId.
	 */
	void RegisterDevice(cl_device_id deviceId);

	/**
	 * Get all the GPU devices and register them.
	 *
	 * @return True if the operation is successful.
	 */
	bool GetDevices();

	DevicesOCL _devices;
};
#endif