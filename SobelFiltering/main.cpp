#include <CL/cl.h>
#include <vector>
#include <map>
#include <chrono>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include "Device.h"

#define Prewit5x5

using uint8 = unsigned char;
using uint32 = unsigned int;

void calculateGlobalSize(size_t localSize[2], size_t globalSize[2])
{
	if (globalSize[0] % localSize[0])
	{
		globalSize[0] = localSize[0] * (globalSize[0] / localSize[0]) + localSize[0];
	}
	if (globalSize[1] % localSize[1])
	{
		globalSize[1] = localSize[1] * (globalSize[1] / localSize[1]) + localSize[1];
	}
}

cl_mem createImage(std::shared_ptr<DeviceOCL> device, uint8* imageData, uint32 width, uint32 height)
{
	// The image format describes how the data will be stored in memory
	cl_image_format format;
	format.image_channel_order = CL_R;			// single channel
	format.image_channel_data_type = CL_FLOAT;	// float data type

	cl_image_desc desc;
	desc.image_type = CL_MEM_OBJECT_IMAGE2D;
	desc.image_width = width;
	desc.image_height = height;
	desc.image_depth = 0;
	desc.image_array_size = 0;
	desc.image_row_pitch = 0;
	desc.image_slice_pitch = 0;
	desc.num_mip_levels = 0;
	desc.num_samples = 0;
	desc.buffer = NULL;
	
	std::vector<float> imageHost(width * height);
	std::copy(imageData, imageData + width * height, imageHost.begin());
	
	cl_int status;
	cl_mem image = clCreateImage(device->GetDeviceContext(), CL_MEM_READ_WRITE, &format, &desc, nullptr, &status);
	if (status != CL_SUCCESS) return nullptr;

	// Copy the source image to the device
	size_t origin[3] = { 0, 0, 0 };				// Offset within the image to copy from
	size_t region[3] = { width, height, 1 };	// Elements to per dimension
	if (clEnqueueWriteImage(device->GetDeviceQueue(), image, CL_FALSE, origin, region, 0, 0, imageHost.data(), 0,
		NULL, NULL) != CL_SUCCESS) return nullptr;

	return image;
}

int main(int argc, char** argv)
{
	cv::Mat image = cv::imread(argv[1], cv::IMREAD_COLOR);   // Read the file
	cv::Mat imageMonochrome;
	cvtColor(image, imageMonochrome, cv::COLOR_BGR2GRAY);
	if (!imageMonochrome.data)                              // Check for invalid input
	{
		std::cout << "Could not get Image!" << std::endl;
		return -1;
	}
	uint32 width = imageMonochrome.size[1];
	uint32 height = imageMonochrome.size[0];

#ifdef Sobel3x3
	uint32 filterWidth = 3;
	uint32 filterSize = filterWidth * filterWidth;  // Assume a square kernel

	float filtery[9] =
	{
	   -1.0,     -2.0,    -1.0,
		0.0,      0.0,     0.0,
	    1.0,      2.0,     1.0
	};
	
	float filterx[9] =
	{
		-1.0,      0.0,      1.0,
		-2.0,      0.0,      2.0,
		-1.0,      0.0,      1.0
	};
#endif

#ifdef Sobel5x5
	// The convolution filter is 7x7
	uint32 filterWidth = 5;
	uint32 filterSize = filterWidth * filterWidth;  // Assume a square kernel
	float filtery[25] =
	{
		2,      1,      0,     -1,     -2,
		2,      1,      0,     -1,     -2,
		4,      2,      0,     -2,     -4,
		2,      1,      0,     -1,     -2,
		2,      1,      0,     -1,     -2
	};

	float filterx[25] =
	{
		2,      2,      4,      2,      2,
		1,      1,      2,      1,      1,
		0,      0,      0,      0,      0,
	   -1,     -1,     -2,     -1,     -1,
	   -2,     -2,     -4,     -2,     -2
	};
#endif

#ifdef Prewit5x5
	// The convolution filter is 7x7
	uint32 filterWidth = 5;
	uint32 filterSize = filterWidth * filterWidth;  // Assume a square kernel
	float filtery[25] =
	{
		2,      1,      0,     -1,     -2,
		2,      1,      0,     -1,     -2,
		2,      1,      0,     -1,     -2,
		2,      1,      0,     -1,     -2,
		2,      1,      0,     -1,     -2
	};

	float filterx[25] =
	{
		2,      2,      2,      2,      2,
		1,      1,      1,      1,      1,
		0,      0,      0,      0,      0,
	   -1,     -1,     -1,     -1,     -1,
	   -2,     -2,     -2,     -2,     -2
	};
#endif

	std::vector<uint8> outputImage(width * height, 0);
	DevicePool devicePool;
	if (devicePool.DeviceCount() < 2)
	{
		std::cout << "Sorry this application requires two GPU cards" << std::endl;
		exit(0);
	}
	cl_mem inputImages[2];
	cl_mem outputImages[2];
	cl_mem filtersx[2];
	cl_mem filtersy[2];
	cl_kernel kernels[2];
	cl_sampler sampler[2];

	uint32 offset[2] = {0, width/2};
	for (uint32 deviceImage = 0; deviceImage < 2; deviceImage++)
	{
		inputImages[deviceImage] = createImage(devicePool[deviceImage], imageMonochrome.data, width, height);
		if (!inputImages[deviceImage])
		{
			std::cout << "Error not a valid image..." << std::endl;
			exit(0);
		}
		outputImages[deviceImage] = createImage(devicePool[deviceImage], outputImage.data(), width, height);
		if (!outputImages[deviceImage])
		{
			std::cout << "Error not a valid image..." << std::endl;
			exit(0);
		}
		// Create the image sampler
		sampler[deviceImage] = clCreateSampler(devicePool[deviceImage]->GetDeviceContext(), CL_FALSE, CL_ADDRESS_NONE,
			CL_FILTER_NEAREST, NULL);
		cl_int status;
		filtersx[deviceImage] = clCreateBuffer(devicePool[deviceImage]->GetDeviceContext(), 0, filterSize * sizeof(float),
			NULL, &status);
		status = clEnqueueWriteBuffer(devicePool[deviceImage]->GetDeviceQueue(), filtersx[deviceImage], CL_FALSE, 0,
			filterSize * sizeof(float), filterx, 0, NULL, NULL);
		filtersy[deviceImage] = clCreateBuffer(devicePool[deviceImage]->GetDeviceContext(), 0, filterSize * sizeof(float),
			NULL, &status);
		status = clEnqueueWriteBuffer(devicePool[deviceImage]->GetDeviceQueue(), filtersy[deviceImage], CL_FALSE, 0,
			filterSize * sizeof(float), filtery, 0, NULL, NULL);

		if (status != CL_SUCCESS)
		{
			std::cout << "Error can't allocate filter buffer..." << std::endl;
			exit(0);
		}
		if (!devicePool[deviceImage]->RegisterProgram(std::string("convolutionProgram"), std::string("C:\\Users\\agalex\\source\\repos\\SobelFiltering\\SobelFiltering\\convolution.cl")))
		{
			std::cout << "Error can't build program..." << std::endl;
			exit(0);
		}
		kernels[deviceImage] = clCreateKernel(devicePool[deviceImage]->GetProgram(std::string("convolutionProgram")), "convolution", &status);
		if (status != CL_SUCCESS)
		{
			std::cout << "Error can't create kernel..." << std::endl;
			exit(0);
		}
	}
	size_t localSize[2] = { 16, 16 };
	size_t globalSize[2][2] = { {width / 2, height}, {width - width / 2, height} };
	calculateGlobalSize(localSize, globalSize[0]);
	calculateGlobalSize(localSize, globalSize[1]);
	uint32 widths[2] = { width / 2, width };
	const auto start = std::chrono::high_resolution_clock::now();

#pragma omp parallel for num_threads(2)
	for (int imagePortion=0; imagePortion<2; imagePortion++)
	{
		cl_int status;
		status  = clSetKernelArg(kernels[imagePortion], 0, sizeof(cl_mem), &inputImages[imagePortion]);
		status |= clSetKernelArg(kernels[imagePortion], 1, sizeof(cl_mem), &outputImages[imagePortion]);
		status |= clSetKernelArg(kernels[imagePortion], 2, sizeof(int), &height);
		status |= clSetKernelArg(kernels[imagePortion], 3, sizeof(int), &widths[imagePortion]);
		status |= clSetKernelArg(kernels[imagePortion], 4, sizeof(cl_mem), &filtersx[imagePortion]);
		status |= clSetKernelArg(kernels[imagePortion], 5, sizeof(cl_mem), &filtersy[imagePortion]);
		status |= clSetKernelArg(kernels[imagePortion], 6, sizeof(int), &filterWidth);
		status |= clSetKernelArg(kernels[imagePortion], 7, sizeof(cl_sampler), &sampler[imagePortion]);
		status |= clSetKernelArg(kernels[imagePortion], 8, sizeof(int), &offset[imagePortion]);

		if (status != CL_SUCCESS)
		{
			std::cout << "Error setting arguments..." << std::endl;
			exit(0);
		}

		// Set the work item dimensions
		auto start = std::chrono::high_resolution_clock::now();
		status = clEnqueueNDRangeKernel(devicePool[imagePortion]->GetDeviceQueue(), kernels[imagePortion], 2, NULL, 
			globalSize[imagePortion], localSize, 0, NULL, NULL);
		if (status != CL_SUCCESS)
		{
			std::cout << "Error executing kernel..." << std::endl;
			exit(0);
		}
		if (clFinish(devicePool[imagePortion]->GetDeviceQueue()) != CL_SUCCESS)
		{
			std::cout << "Something went wrong in Finalization..." << std::endl;
			exit(0);
		}
		auto stop = std::chrono::high_resolution_clock::now();
		auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
#pragma omp critical
		std::cout << "duration of kernel on " << devicePool[imagePortion]->GetDeviceName() << " : " << duration.count() << 
			" in microseconds." << std::endl;
	}

	const auto stop = std::chrono::high_resolution_clock::now();
	const auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
	std::cout << "parallel duration:" << duration.count() << std::endl;
	std::vector<std::vector<float>> outputImagesHost(2);
	outputImagesHost[0].resize(width* height);
	outputImagesHost[1].resize(width* height);
	std::vector<float> wholeImage(width* height, 0.0f);

	for (int imagePortion = 0; imagePortion < 2; imagePortion++)
	{
		cl_int status;
		size_t origin[3] = { 0, 0, 0 };				// Offset within the image to copy from
		size_t region[3] = { width, height, 1 };	// Elements to per dimension
		status = clEnqueueReadImage(devicePool[imagePortion]->GetDeviceQueue(), outputImages[imagePortion], CL_TRUE, origin,
			region, 0, 0, outputImagesHost[imagePortion].data(), 0, NULL, NULL);
		if (status != CL_SUCCESS)
		{
			std::cout << "Copy of Image failed:" << std::endl;
			exit(0);
		}
		for (uint32 i = 0; i < height * width; i++)
		{
			wholeImage[i] += std::clamp(outputImagesHost[imagePortion][i], 0.0f, 255.0f);
		}
	}

	for (uint32 deviceIndex = 0; deviceIndex < 2; deviceIndex++)
	{
		clReleaseMemObject(inputImages[deviceIndex]);
		clReleaseMemObject(outputImages[deviceIndex]);
		clReleaseMemObject(filtersx[deviceIndex]);
		clReleaseMemObject(filtersy[deviceIndex]);
		clReleaseKernel(kernels[deviceIndex]);
		clReleaseSampler(sampler[deviceIndex]);
	}
	
	std::copy(wholeImage.begin(), wholeImage.end(), imageMonochrome.data);
	namedWindow("Display window", cv::WINDOW_NORMAL);// Create a window for display.
	imshow("Display window", imageMonochrome);                   // Show our image inside it.

	cv::waitKey(0);                                          // Wait for a keystroke in the window
}
